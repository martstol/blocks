#version 450 core

in vec2 uv;
in vec3 n;
in vec3 l;

out vec3 outColor;

float ambient() {
	return 0.05;
}

float emissive() {
	return 0;
}

float diffuse() {
	return max(0, dot(n, l));
}

float specular() {
	return 0;
}

float light() {
	return min(ambient() + emissive() + diffuse() + specular(), 1);
}

vec3 illuminate(vec3 color) {
	return light() * color;
}

void main() {
	outColor = illuminate(vec3(uv, 0));
}