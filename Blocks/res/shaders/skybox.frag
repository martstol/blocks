#version 450 core

in vec3 uv;

layout(location = 2) uniform samplerCube tex;

out vec4 outColor;

void main() {
	outColor = texture(tex, uv);
}