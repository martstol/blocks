#version 450 core

layout(location = 0) in vec3 position;

layout(location = 0) uniform mat4 vp;
layout(location = 1) uniform vec3 translate;

out vec3 uv;

void main() {
	vec4 pos = vp * vec4(position + translate, 1.0);
	gl_Position = pos.xyww;
	uv = position;
}