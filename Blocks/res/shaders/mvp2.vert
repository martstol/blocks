#version 450 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec3 normal;

layout(location = 0) uniform mat4 vp;
layout(location = 1) uniform vec3 translation;
layout(location = 2) uniform vec3 lightPos;

out vec2 uv;
out vec3 n;
out vec3 l;

void main() {
	vec3 pos = position + translation;
	gl_Position = vp * vec4(pos, 1.0);
	uv = texCoord;
	n = normal;
	l = lightPos - pos;
}