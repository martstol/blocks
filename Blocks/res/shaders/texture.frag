#version 450 core

in vec2 uv;
in vec3 n;
in vec3 l;

layout(location = 3) uniform sampler2D tex;

out vec4 outColor;

float distance() {
	float d = length(l);
	return 1 / (1 + 0.05*d + 0.005*d*d);
}

float ambient() {
	return 0.05;
}

float emissive() {
	return 0;
}

float diffuse() {
	return dot(n, normalize(l));
}

float specular() {
	return 0;
}

float light() {
	return ambient() + emissive() + diffuse() + specular();
}

vec4 illuminate(vec4 color) {
	return distance() * light() * color;
}

void main() {
	outColor = illuminate(texture(tex, uv));
}