#pragma once

#ifndef _MODEL_H_
#define _MODEL_H_

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <vector>

#include "graphics/vertex.h"

namespace Blocks {

	class Model {
	private:
		GLuint vao;
		GLuint vbo;
		GLuint ebo;
		GLenum drawMode;
		GLsizei numIndices;

	public:
		Model(std::vector<Vertex> const& vertices, std::vector<GLuint> const& indices, GLenum drawMode = GL_TRIANGLES);
		~Model();

		Model(Model &other) = delete;
		Model & operator=(Model &other) = delete;

		Model(Model &&other);
		Model & operator=(Model &&other);

		void setVertexAttribute(GLint attrib, GLint size, GLenum type, GLboolean normalized, GLsizei stride, size_t offset);

		void render() const;
	};

}

#endif