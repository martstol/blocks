#pragma once

#ifndef _RESOURCES_H_
#define _RESOURCES_H_

#include "graphics/model.h"
#include "graphics/shader.h"
#include "graphics/texture.h"
#include "graphics/skybox.h"
#include "game/block.h"

#include <array>

namespace Blocks {

	typedef std::uint8_t BlockId;

	namespace BlockIds {
		static BlockId const Air = 0;
		static BlockId const Stone = 1;
	}

	class ResourceManager {
	private:
		std::array<Block, 1 << (8 * sizeof(BlockId))> blocks;

	public:
		ShaderProgram blockShader;
		Model blockModel;
		Texture stoneTexture;
		Skybox skybox;

		ResourceManager();

		Block const& getBlock(BlockId id) const { return blocks[id]; };

	};

}

#endif