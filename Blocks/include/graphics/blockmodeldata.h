#pragma once

#ifndef _BLOCKMODELDATA_H_
#define _BLOCKMODELDATA_H_

#include "graphics/vertex.h"
#include <GL/glew.h>
#include <vector>

namespace Blocks {
	namespace BlockModelData {
		static std::vector<Vertex> const vertices = {
			// Front
			Vertex(glm::vec3(0, 0, 0), glm::vec2(1, 1), glm::vec3(0, 0, -1)),
			Vertex(glm::vec3(0, 1, 0), glm::vec2(1, 0), glm::vec3(0, 0, -1)),
			Vertex(glm::vec3(1, 1, 0), glm::vec2(0, 0), glm::vec3(0, 0, -1)),
			Vertex(glm::vec3(1, 0, 0), glm::vec2(0, 1), glm::vec3(0, 0, -1)),

			// Back
			Vertex(glm::vec3(1, 0, 1), glm::vec2(1, 1), glm::vec3(0, 0, 1)),
			Vertex(glm::vec3(1, 1, 1), glm::vec2(1, 0), glm::vec3(0, 0, 1)),
			Vertex(glm::vec3(0, 1, 1), glm::vec2(0, 0), glm::vec3(0, 0, 1)),
			Vertex(glm::vec3(0, 0, 1), glm::vec2(0, 1), glm::vec3(0, 0, 1)),

			// Top
			Vertex(glm::vec3(0, 1, 0), glm::vec2(1, 1), glm::vec3(0, 1, 0)),
			Vertex(glm::vec3(0, 1, 1), glm::vec2(1, 0), glm::vec3(0, 1, 0)),
			Vertex(glm::vec3(1, 1, 1), glm::vec2(0, 0), glm::vec3(0, 1, 0)),
			Vertex(glm::vec3(1, 1, 0), glm::vec2(0, 1), glm::vec3(0, 1, 0)),

			// Bottom
			Vertex(glm::vec3(0, 0, 1), glm::vec2(1, 1), glm::vec3(0, -1, 0)),
			Vertex(glm::vec3(0, 0, 0), glm::vec2(1, 0), glm::vec3(0, -1, 0)),
			Vertex(glm::vec3(1, 0, 0), glm::vec2(0, 0), glm::vec3(0, -1, 0)),
			Vertex(glm::vec3(1, 0, 1), glm::vec2(0, 1), glm::vec3(0, -1, 0)),

			// Right
			Vertex(glm::vec3(0, 0, 1), glm::vec2(1, 1), glm::vec3(-1, 0, 0)),
			Vertex(glm::vec3(0, 1, 1), glm::vec2(1, 0), glm::vec3(-1, 0, 0)),
			Vertex(glm::vec3(0, 1, 0), glm::vec2(0, 0), glm::vec3(-1, 0, 0)),
			Vertex(glm::vec3(0, 0, 0), glm::vec2(0, 1), glm::vec3(-1, 0, 0)),

			// Left
			Vertex(glm::vec3(1, 0, 0), glm::vec2(1, 1), glm::vec3(1, 0, 0)),
			Vertex(glm::vec3(1, 1, 0), glm::vec2(1, 0), glm::vec3(1, 0, 0)),
			Vertex(glm::vec3(1, 1, 1), glm::vec2(0, 0), glm::vec3(1, 0, 0)),
			Vertex(glm::vec3(1, 0, 1), glm::vec2(0, 1), glm::vec3(1, 0, 0)),
		};

		static std::vector<GLuint> const indices = {
			0, 1, 2,		0, 2, 3,
			4, 5, 6,		4, 6, 7,
			8, 9, 10,		8, 10, 11,
			12, 13, 14,		12, 14, 15,
			16, 17, 18,		16, 18, 19,
			20, 21, 22,		20, 22, 23,
		};
	}
}

#endif