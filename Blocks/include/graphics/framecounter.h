#pragma once

#ifndef _FRAMECOUNTER_H_
#define _FRAMECOUNTER_H_

#include <chrono>
#include <SFML/Window.hpp>

namespace Blocks {
	class FrameCounter {
	private:
		int frames;
		std::chrono::steady_clock::time_point lastFpsUpdate;
	public:
		FrameCounter();
		void update(sf::Window &window);
	};
}

#endif