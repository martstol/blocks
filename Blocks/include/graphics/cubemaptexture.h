#pragma once

#ifndef _CUBEMAP_TEXTURE_H_
#define _CUBEMAP_TEXTURE_H_

#include <GL/glew.h>
#include <string>

namespace Blocks {

	class CubemapTexture {
	private:
		GLuint tex;

	public:
		CubemapTexture(std::string const& posXFilename,
			std::string const& negXFilename,
			std::string const& posYFilename,
			std::string const& negYFilename,
			std::string const& posZFilename,
			std::string const& negZFilename);
		~CubemapTexture();

		CubemapTexture(CubemapTexture & other) = delete;
		CubemapTexture& operator=(CubemapTexture & other) = delete;

		CubemapTexture(CubemapTexture && other);
		CubemapTexture& operator=(CubemapTexture && other);

		void enable(GLenum textureUnit) const;
	};

}

#endif