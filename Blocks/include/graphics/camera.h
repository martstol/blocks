#pragma once

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <glm/glm.hpp>
#include <SFML/Window.hpp>

namespace Blocks {
	class Camera {
	private:
		float fieldOfView;
		float zNear;
		float zFar;

		glm::vec3 pos;
		glm::vec3 up;
		glm::vec2 angle;

		glm::vec2 sensitivity;

		int screenWidth;
		int screenHeight;
	public:
		Camera(int screenWidth, int screenHeight);
		Camera(sf::Vector2u screenSize) : Camera(screenSize.x, screenSize.y) {};

		glm::mat4 lookAt() const;
		glm::mat4 perspective() const;
		glm::mat4 getViewProjMatrix() const;

		void setScreenSize(int screenWidth, int screenHeight);
		int getScreenWidth() { return screenWidth; }
		int getScreenHeight() { return screenHeight; }

		void turn(float ax, float ay);
		void turn(glm::vec2 angle);

		void setDirection(float horizontal, float vertical);
		void setDirection(glm::vec2 angle);

		glm::vec3 getDirection() const;
		static glm::vec3 calcDirection(float ax, float ay);

		void move(float dx, float dy, float dz);
		void move(glm::vec3 delta);

		void setPosition(float x, float y, float z);
		void setPosition(glm::vec3 pos);

		glm::vec3 getPosition() const;

		void forward(float rate = 1.0f);
		void backward(float rate = 1.0f);
		void left(float rate = 1.0f);
		void right(float rate = 1.0f);
		void upward(float rate = 1.0f);
		void downward(float rate = 1.0f);
	};
}

#endif