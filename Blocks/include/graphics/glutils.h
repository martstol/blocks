#pragma once

#ifndef _GLUTILS_H_
#define _GLUTILS_H_

namespace Blocks {
	void printGLInfo();
	void initializeGlew();
	void setupGL();
}

#endif