#pragma once

#ifndef _SHADER_H_
#define _SHADER_H_

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <string>
#include <map>
#include <functional>

namespace Blocks {

	class ShaderProgram {
	private:
		GLuint programId;

		GLuint loadShader(GLenum type, std::string const& filename);
		GLuint linkProgram(GLuint vertexShaderId, GLuint fragmentShaderId);
		void errorCheck(GLuint id, GLenum parameter, std::string const& name,
			std::function<void(GLuint,GLenum,GLint*)> parmFunc, 
			std::function<void(GLuint,GLsizei,GLsizei*,GLchar*)> logFunc) const;
	public:
		ShaderProgram(std::string const& vertFilename, std::string const& fragFilename);
		~ShaderProgram();

		ShaderProgram(ShaderProgram & other) = delete;
		ShaderProgram & operator=(ShaderProgram & other) = delete;

		ShaderProgram(ShaderProgram && other) : programId(other.programId) {
			other.programId = 0;
		};

		ShaderProgram & operator=(ShaderProgram && other) {
			glDeleteProgram(programId);
			programId = other.programId; other.programId = 0;
			return *this;
		};

		void enable() const;
		void setFragmentDataLocation(std::string const& name, GLuint location) const;
		void setUniformValue(GLint location, glm::mat4 const& mat) const;
		void setUniformValue(GLint location, glm::vec3 const& vec) const;
		void setUniformValue(GLint location, GLint value) const;
	};

}

#endif