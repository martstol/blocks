﻿#pragma once

#ifndef _SKYBOX_H_
#define _SKYBOX_H_

#include "graphics/cubemaptexture.h"
#include "graphics/model.h"
#include "graphics/shader.h"

#include <glm/glm.hpp>

#include <string>

namespace Blocks {

	class Skybox {
	private:
		CubemapTexture texture;
		ShaderProgram shader;
		Model model;

	public:
		Skybox(std::string const& posXFilename,
			std::string const& negXFilename,
			std::string const& posYFilename,
			std::string const& negYFilename,
			std::string const& posZFilename,
			std::string const& negZFilename,
			std::string vertexShader,
			std::string fragmentShader);
		void render(glm::mat4 const& vp, glm::vec3 const& pos) const;

	};

}

#endif