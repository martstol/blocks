#pragma once

#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <glm/glm.hpp>

namespace Blocks {
	class Vertex {
	private:
		glm::vec3 position;
		glm::vec2 texCoord;
		glm::vec3 normal;

	public:
		Vertex(glm::vec3 const& position, glm::vec2 const& texCoord, glm::vec3 const& normal) : position(position), texCoord(texCoord), normal(normal) {};

		static size_t positionOffset() { return offsetof(Vertex, position); };
		static size_t texCoordOffset() { return offsetof(Vertex, texCoord); };
		static size_t normalOffset() { return offsetof(Vertex, normal); };
	};
}

#endif