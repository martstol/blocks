#pragma once

#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <GL/glew.h>

#include <string>

namespace Blocks {

	class Texture {
	private:
		GLuint tex;
	public:
		Texture(std::string const& name);
		~Texture();

		Texture(Texture & other) = delete;
		Texture& operator=(Texture & other) = delete;

		Texture(Texture && other);
		Texture& operator=(Texture && other);

		void enable(GLenum textureUnit) const;
	};

}

#endif