#pragma once

#ifndef _UTILS_H_
#define _UTILS_H_

#include <string>
#include <vector>

namespace Blocks {
	std::string readFile(std::string const& filename);

	struct Image {
		std::vector<unsigned char> pixels;
		unsigned int width;
		unsigned int height;
	};

	Image readImage(std::string const& filename);
}

#endif