#pragma once

#ifndef _GAME_H_
#define _GAME_H_

#include "game/world.h"
#include "game/inputhandler.h"
#include "graphics/camera.h"
#include "game/keys.h"
#include "graphics/resources.h"

#include <SFML/Window.hpp>

namespace Blocks {
	class Game {
	private:
		bool running;
		World world;
		Camera camera;
		ResourceManager resources;
		Keys keys;
		InputHandler inputHandler;

	public:
		Game(sf::Window const& window);
		bool isRunning() const;
		void start();
		void stop();
		void update(sf::Window &window);
		void render(sf::Window &window);
		void handleEvent(sf::Event const& event);
	};
}

#endif