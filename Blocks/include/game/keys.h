#pragma once

#ifndef _KEYS_H_
#define _KEYS_H_

#include <glm/glm.hpp>

namespace Blocks {

	class Key {
	private:
		bool pressed;
	public:
		Key() : pressed(false) {}
		void press() { pressed = true; }
		void release() { pressed = false; }
		void setPressed(bool p) { pressed = p; }
		bool isPressed() const { return pressed; }
	};

	class Mouse {
	public:
		Key left;
		Key right;
		glm::vec2 pos;
	};

	class Keys {
	public:
		Key up;
		Key down;
		Key left;
		Key right;
		Key space;
		Key ctrl;
		Key shift;
		Mouse mouse;
	};

}

#endif