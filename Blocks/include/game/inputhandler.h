#pragma once

#ifndef _INPUTHANDLER_H_
#define _INPUTHANDLER_H_

#include "game/keys.h"

#include <unordered_map>
#include <memory>

#include <SFML/Window.hpp>

namespace Blocks {
	class InputHandler {
	private:
		Keys * keys;
		std::unordered_map<sf::Keyboard::Key, Key*> keyBindings;
		std::unordered_map<sf::Mouse::Button, Key*> mouseBindings;

		void handleMouseMovedEvent(sf::Event const &event);
		void handleMouseButtonPressedEvent(sf::Event const &event);
		void handleMouseButtonReleasedEvent(sf::Event const &event);
		void handleKeyPressedEvent(sf::Event const &event);
		void handleKeyReleasedEvent(sf::Event const &event);
	public:
		InputHandler(Keys &keys);
		void handleEvent(sf::Event const &event);
	};
}

#endif