#pragma once

#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <glm/glm.hpp>

#include "graphics/resources.h"

namespace Blocks {

	class World;

	class Entity {
	private:
		glm::vec3 position;
		glm::vec2 direction;
	public:
		Entity(glm::vec3 const &position, glm::vec2 const &direction);
		void update(World * world);
		void render(glm::mat4 const &vp, ResourceManager &resources);
	};

}

#endif