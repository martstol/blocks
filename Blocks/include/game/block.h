#pragma once

#ifndef _BLOCK_H_
#define _BLOCK_H_

#include <glm/glm.hpp>

#include "graphics/texture.h"
#include "graphics/shader.h"
#include "graphics/model.h"

namespace Blocks {

	class ResourceManager;

	class Block {
	private:
		bool solid;
		Texture const* texture;
		Texture const* bumpmap;
		ShaderProgram const* shader;
		Model const* model;

	public:
		Block(bool solid = false, 
			Texture const* texture = nullptr, 
			ShaderProgram const* shader = nullptr, 
			Model const* model = nullptr,
			Texture const* bumpmap = nullptr) : 
			solid(solid), texture(texture), shader(shader), model(model), bumpmap(bumpmap) {}

		bool isSolid() { return solid; }

		void render(glm::vec3 const& pos, ResourceManager &resources) const;
	};

}

#endif