#pragma once

#ifndef _WORLD_H_
#define _WORLD_H_

#include "game/chunk.h"
#include "graphics/resources.h"
#include "game/entity.h"
#include "graphics/camera.h"

#include <glm/glm.hpp>

#include <vector>

namespace Blocks {
	class World {
	private:
		Chunk chunk;
		std::vector<Entity> entities;
	public:
		World();
		void update();
		void render(Camera const& camera, ResourceManager &resources);
	};
}

#endif