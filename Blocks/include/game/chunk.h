#pragma once

#ifndef _CHUNK_H_
#define _CHUNK_H_

#include "game/block.h"
#include "graphics/resources.h"

#include <glm/glm.hpp>

#include <array>
#include <algorithm>

namespace Blocks {

	class ChunkEntry {
	private:
		BlockId blockId;
	public:
		ChunkEntry() : blockId(BlockIds::Air) {}
		explicit ChunkEntry(BlockId blockId) : blockId(blockId) {}

		BlockId getBlockId() const { return blockId; }
		bool isVisible() const { return blockId != BlockIds::Air; }
	};

	class Chunk {
	public:
		static size_t const Width = 32;
		static size_t const Depth = 32;
		static size_t const Height = 2;

		Chunk() { std::fill(content.begin(), content.end(), ChunkEntry(BlockIds::Stone)); }

		BlockId getBlockId(size_t x, size_t y, size_t z) { return content[calcId(x, y, z)].getBlockId(); }
		bool isBlockVisible(size_t x, size_t y, size_t z) { return content[calcId(x, y, z)].isVisible(); }
		void setBlock(size_t x, size_t y, size_t z, BlockId id) { content[calcId(x, y, z)] = ChunkEntry(id); }

		void render(int i, int j, ResourceManager &resources);

	private:
		size_t calcId(size_t x, size_t y, size_t z) { return x + y*Width + z*Width*Height; }
		std::array<ChunkEntry, Width*Height*Depth> content;
	};

}

#endif