#include "graphics/texture.h"
#include "utils.h"

#include <vector>
#include <stdexcept>

Blocks::Texture::Texture(std::string const& filename) {
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	Image img = readImage(filename);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.width, img.height, 0, GL_RGB, GL_UNSIGNED_BYTE, img.pixels.data());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
}

Blocks::Texture::~Texture() {
	glDeleteTextures(1, &tex);
}

Blocks::Texture::Texture(Texture && other) {
	tex = other.tex; other.tex = 0;
}

Blocks::Texture & Blocks::Texture::operator=(Texture && other) {
	glDeleteTextures(1, &tex);
	tex = other.tex; other.tex = 0;
	return *this;
}

void Blocks::Texture::enable(GLenum textureUnit) const {
	glActiveTexture(textureUnit);
	glBindTexture(GL_TEXTURE_2D, tex);
}
