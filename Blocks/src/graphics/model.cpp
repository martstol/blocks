#include "graphics/model.h"

#include <algorithm>
#include <iostream>

Blocks::Model::Model(std::vector<Vertex> const& vertices, 
	std::vector<GLuint> const& indices, GLenum drawMode) : numIndices(indices.size()), drawMode(drawMode) {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*indices.size(), indices.data(), GL_STATIC_DRAW);
	
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Blocks::Model::setVertexAttribute(GLint attrib, GLint size, GLenum type, GLboolean normalized, GLsizei stride, size_t offset) {
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	glEnableVertexAttribArray(attrib);
	glVertexAttribPointer(attrib, size, type, normalized, stride, (GLvoid const*)offset);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Blocks::Model::~Model() {
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);
}

Blocks::Model::Model(Model &&other) {
	vao = other.vao; other.vao = 0;
	ebo = other.ebo; other.ebo = 0;
	vbo = other.vbo; other.vbo = 0;

	drawMode = other.drawMode;
	numIndices = other.numIndices;
}

Blocks::Model& Blocks::Model::operator=(Model &&other) {
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ebo);

	vao = other.vao; other.vao = 0;
	ebo = other.ebo; other.ebo = 0;
	vbo = other.vbo; other.vbo = 0;

	drawMode = other.drawMode;
	numIndices = other.numIndices;

	return *this;
}

void Blocks::Model::render() const {
	glBindVertexArray(vao);
	glDrawElements(drawMode, numIndices, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}