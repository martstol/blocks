#include "graphics/resources.h"
#include "graphics/blockmodeldata.h"

Blocks::ResourceManager::ResourceManager() : 
	blockModel(Blocks::BlockModelData::vertices, Blocks::BlockModelData::indices),
	blockShader("res/shaders/mvp2.vert", "res/shaders/texture.frag"), stoneTexture("res/textures/stone.png"),
	skybox("res/textures/skybox_right.png", "res/textures/skybox_left.png", "res/textures/skybox_top.png", 
		"res/textures/skybox_bottom.png", "res/textures/skybox_front.png", "res/textures/skybox_back.png", 
		"res/shaders/skybox.vert", "res/shaders/skybox.frag") {

	blockModel.setVertexAttribute(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), Vertex::positionOffset());
	blockModel.setVertexAttribute(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), Vertex::texCoordOffset());
	blockModel.setVertexAttribute(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), Vertex::normalOffset());

	blocks[BlockIds::Air] = Block();
	blocks[BlockIds::Stone] = Block(true, &stoneTexture, &blockShader, &blockModel);
}