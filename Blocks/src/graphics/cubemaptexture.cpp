#include "graphics/cubemaptexture.h"
#include "utils.h"

#include <vector>
#include <stdexcept>
#include <iostream>

Blocks::CubemapTexture::CubemapTexture(
	std::string const& posXFilename,
	std::string const& negXFilename,
	std::string const& posYFilename,
	std::string const& negYFilename,
	std::string const& posZFilename,
	std::string const& negZFilename) {
	
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
	
	auto load = [](GLenum target, std::string const& filename) {
		Image img = readImage(filename);
		glTexImage2D(target, 0, GL_RGB, img.width, img.height, 0, GL_RGB, GL_UNSIGNED_BYTE, img.pixels.data());
	};

	load(GL_TEXTURE_CUBE_MAP_POSITIVE_X, posXFilename);
	load(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, negXFilename);
	load(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, posYFilename);
	load(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, negYFilename);
	load(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, posZFilename);
	load(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, negZFilename);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

Blocks::CubemapTexture::~CubemapTexture() {
	glDeleteTextures(1, &tex);
}

Blocks::CubemapTexture::CubemapTexture(CubemapTexture && other) : tex(other.tex) {
	other.tex = 0;
}

Blocks::CubemapTexture & Blocks::CubemapTexture::operator=(CubemapTexture && other) {
	glDeleteTextures(1, &tex);
	tex = other.tex; other.tex = 0;
	return *this;
}

void Blocks::CubemapTexture::enable(GLenum textureUnit) const {
	glActiveTexture(textureUnit);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
}
