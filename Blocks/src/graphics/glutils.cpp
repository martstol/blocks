#include "graphics/glutils.h"

#include <GL/glew.h>
#include <cstdio>
#include <stdexcept>

void Blocks::printGLInfo() {
	std::printf("OpenGL Context Information:\n");

	int versionMajor, versionMinor;
	glGetIntegerv(GL_MAJOR_VERSION, &versionMajor);
	glGetIntegerv(GL_MINOR_VERSION, &versionMinor);
	std::printf("GL Version:\t %d.%d\n", versionMajor, versionMinor);

	std::printf("GL Vendor:\t %s\n", glGetString(GL_VENDOR));

	std::printf("GL Renderer:\t %s\n", glGetString(GL_RENDERER));

	std::printf("GLSL Version:\t %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
}

void Blocks::initializeGlew() {
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error("Initialize glew failed");
	}
}

void Blocks::setupGL() {
	glClearColor(.1f, .1f, .1f, .0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
}
