#include "graphics/skybox.h"
#include "graphics/vertex.h"

#include <vector>

using Blocks::Vertex;
using glm::vec3;
using glm::vec2;

static std::vector<Vertex> const vertices = {
	Vertex(vec3(-1, -1, -1), vec2(0), vec3(0)),
	Vertex(vec3( 1, -1, -1), vec2(0), vec3(0)),
	Vertex(vec3( 1,  1, -1), vec2(0), vec3(0)),
	Vertex(vec3(-1,  1, -1), vec2(0), vec3(0)),
	Vertex(vec3(-1, -1,  1), vec2(0), vec3(0)),
	Vertex(vec3( 1, -1,  1), vec2(0), vec3(0)),
	Vertex(vec3( 1,  1,  1), vec2(0), vec3(0)),
	Vertex(vec3(-1,  1,  1), vec2(0), vec3(0)),
};

static std::vector<GLuint> const indices = {
	2, 1, 5, 6,	//right		(east)
	3, 7, 4, 0,	//left		(west)
	7, 6, 5, 4,	//top		(up)
	0, 1, 2, 3,	//bottom	(down)
	3, 2, 6, 7,	//front		(north)
	1, 0, 4, 5	//back		(south)
};

Blocks::Skybox::Skybox(
	std::string const& posXFilename,
	std::string const& negXFilename,
	std::string const& posYFilename,
	std::string const& negYFilename,
	std::string const& posZFilename,
	std::string const& negZFilename,
	std::string vertexShader,
	std::string fragmentShader) :
	texture(posXFilename, negXFilename, posYFilename, negYFilename, posZFilename, negZFilename),
	shader(vertexShader, fragmentShader), model(vertices, indices, GL_QUADS) {

	model.setVertexAttribute(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), Vertex::positionOffset());
}

void Blocks::Skybox::render(glm::mat4 const& vp, glm::vec3 const& pos) const {
	glDisable(GL_DEPTH_TEST);
	shader.enable();
	shader.setUniformValue(0, vp);
	shader.setUniformValue(1, pos);
	texture.enable(GL_TEXTURE0);
	shader.setUniformValue(2, 0);
	model.render();
	glEnable(GL_DEPTH_TEST);
}