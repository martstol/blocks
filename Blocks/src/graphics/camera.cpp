#include "graphics/camera.h"

#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/common.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>

Blocks::Camera::Camera(int sw, int sh) : screenWidth(sw), screenHeight(sh),
	fieldOfView(45.0f), zNear(0.1f), zFar(100.0f), pos(0.0f), up(0.0f, 1.0f, 0.0f),
	angle(0.0f, 0.5f), sensitivity(1.0f) {};

glm::mat4 Blocks::Camera::perspective() const {
	return glm::perspective(fieldOfView, (((float)screenWidth) / screenHeight), zNear, zFar);
}

glm::mat4 Blocks::Camera::lookAt() const {
	return glm::lookAt(pos, pos+getDirection(), up);
}

glm::mat4 Blocks::Camera::getViewProjMatrix() const {
	return perspective() * lookAt();
}

void Blocks::Camera::setScreenSize(int sw, int sh) {
	screenWidth = sw;
	screenHeight = sh;
}

void Blocks::Camera::turn(float horizontal, float vertical) {
	angle.x = glm::fract(angle.x + sensitivity.x * horizontal * glm::one_over_two_pi<float>());
	angle.y = glm::clamp(angle.y + sensitivity.y * vertical * glm::one_over_pi<float>(), 0.0001f, 0.9999f);
}

void Blocks::Camera::turn(glm::vec2 angle) {
	turn(angle.x, angle.y);
}

void Blocks::Camera::setDirection(float horizontal, float vertical) {
	angle.x = glm::fract(horizontal * glm::one_over_two_pi<float>());
	angle.y = glm::fract(vertical * glm::one_over_two_pi<float>());
}

void Blocks::Camera::setDirection(glm::vec2 angle) {
	this->angle = glm::fract(angle * glm::one_over_two_pi<float>());
}

glm::vec3 Blocks::Camera::getDirection() const {
	float ax = glm::two_pi<float>() * angle.x;
	float ay = glm::pi<float>() * (angle.y - 0.5f);
	return calcDirection(ax, ay);
}

glm::vec3 Blocks::Camera::calcDirection(float ax, float ay) {
	glm::vec3 dir(0.0f, 0.0f, 1.0f);
	dir = glm::rotateY(dir, ax);

	glm::vec3 axis(dir.z, 0.0f, -dir.x);
	dir = glm::rotate(dir, ay, axis);

	return dir;
}

void Blocks::Camera::move(float dx, float dy, float dz) {
	pos.x += dx;
	pos.y += dy;
	pos.z += dz;
}

void Blocks::Camera::move(glm::vec3 delta) {
	pos += delta;
}

void Blocks::Camera::setPosition(float x, float y, float z) {
	pos.x = x;
	pos.y = y;
	pos.z = z;
}

void Blocks::Camera::setPosition(glm::vec3 pos) {
	this->pos = pos;
}

void Blocks::Camera::forward(float rate) {
	pos += rate * getDirection();
}

void Blocks::Camera::backward(float rate) {
	pos -= rate * getDirection();
}

void Blocks::Camera::left(float rate) {
	float ax = glm::two_pi<float>() * (angle.x + 0.5f);
	glm::vec3 dir = calcDirection(ax, 0.0f);

	pos.x -= rate * dir.z;
	pos.z += rate * dir.x;
}

void Blocks::Camera::right(float rate) {
	float ax = glm::two_pi<float>() * (angle.x + 0.5f);
	glm::vec3 dir = calcDirection(ax, 0.0f);

	pos.x += rate * dir.z;
	pos.z -= rate * dir.x;
}

void Blocks::Camera::upward(float rate) {
	pos += rate * up;
}

void Blocks::Camera::downward(float rate) {
	pos -= rate * up;
}

glm::vec3 Blocks::Camera::getPosition() const {
	return pos;
}
