#include "graphics/shader.h"
#include "utils.h"

#include <stdexcept>
#include <iostream>
#include <vector>

#include <glm/gtc/type_ptr.hpp>

Blocks::ShaderProgram::ShaderProgram(std::string const& vertFilename, std::string const& fragFilename) {
	GLuint vertexShaderId = loadShader(GL_VERTEX_SHADER, vertFilename);
	GLuint fragmentShaderId = loadShader(GL_FRAGMENT_SHADER, fragFilename);

	programId = linkProgram(vertexShaderId, fragmentShaderId);

	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);
}

Blocks::ShaderProgram::~ShaderProgram() {
	glDeleteProgram(programId);
}

void Blocks::ShaderProgram::setUniformValue(GLint location, glm::mat4 const& mat) const {
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
}

void Blocks::ShaderProgram::setUniformValue(GLint location, glm::vec3 const& vec) const {
	glUniform3fv(location, 1, glm::value_ptr(vec));
}

void Blocks::ShaderProgram::setUniformValue(GLint location, GLint value) const {
	glUniform1i(location, value);
}

GLuint Blocks::ShaderProgram::loadShader(GLenum type, std::string const& filename) {
	GLuint shaderId = glCreateShader(type);

	std::string shaderCode = Blocks::readFile(filename);
	GLint length = shaderCode.length();
	GLchar const * data = shaderCode.c_str();

	glShaderSource(shaderId, 1, &data, &length);
	glCompileShader(shaderId);

	errorCheck(shaderId, GL_COMPILE_STATUS, filename, glGetShaderiv, glGetShaderInfoLog);

	return shaderId;
}

GLuint Blocks::ShaderProgram::linkProgram(GLuint vertexShaderId, GLuint fragmentShaderId) {
	GLuint programId = glCreateProgram();

	glAttachShader(programId, vertexShaderId);
	glAttachShader(programId, fragmentShaderId);
	glLinkProgram(programId);

	errorCheck(programId, GL_LINK_STATUS, "linking", glGetProgramiv, glGetProgramInfoLog);

	return programId;
}

void Blocks::ShaderProgram::errorCheck(
	GLuint id, GLenum parameter, std::string const& name,
	std::function<void(GLuint, GLenum, GLint*)> parmFunc,
	std::function<void(GLuint, GLsizei, GLsizei*, GLchar*)> logFunc) const {

	GLint status = 0;
	parmFunc(id, parameter, &status);
	if (status == GL_FALSE) {
		int logLength;
		parmFunc(id, GL_INFO_LOG_LENGTH, &logLength);

		std::vector<char> log(logLength);
		logFunc(id, logLength, nullptr, log.data());

		throw std::runtime_error("Error (" + name + "): " + std::string(log.begin(), log.end()));
	}
}

void Blocks::ShaderProgram::enable() const {
	glUseProgram(programId);
}
