#include "graphics/framecounter.h"

#include <iostream>
#include <sstream>

Blocks::FrameCounter::FrameCounter() : frames(0), lastFpsUpdate(std::chrono::steady_clock::now()) {
}

void Blocks::FrameCounter::update(sf::Window &window) {
	static std::chrono::milliseconds oneSec(1000);

	frames++;
	auto now = std::chrono::steady_clock::now();
	auto diff = now - lastFpsUpdate;
	if (diff > oneSec) {
		std::stringstream ss;
		ss << "Blocks " << frames;
		window.setTitle(ss.str());
		lastFpsUpdate += oneSec;
		frames = 0;
	}
}