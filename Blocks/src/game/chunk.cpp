#include "game/chunk.h"

void Blocks::Chunk::render(int i, int j, ResourceManager &resources) {
	for (size_t z = 0; z < Depth; z++) {
		for (size_t y = 0; y < Height; y++) {
			for (size_t x = 0; x < Width; x++) {
				if (isBlockVisible(x, y, z)) {
					Block const& block = resources.getBlock(getBlockId(x, y, z));
					block.render(glm::vec3(x+i, y, z+j), resources);

				}
			}
		}
	}
}


