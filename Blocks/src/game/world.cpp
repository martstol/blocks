#include "game/world.h"

#include <algorithm>

Blocks::World::World() {

}

void Blocks::World::update() {
	std::for_each(entities.begin(), entities.end(),
		[this](Entity &entity) {entity.update(this); });
}

void Blocks::World::render(Camera const& camera, ResourceManager &resources) {
	glm::mat4 vp = camera.getViewProjMatrix();

	resources.skybox.render(vp, camera.getPosition());

	resources.blockShader.enable();
	resources.blockShader.setUniformValue(0, vp);
	resources.blockShader.setUniformValue(2, camera.getPosition());
	chunk.render(0, 0, resources);
}
