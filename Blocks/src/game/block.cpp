#include "game/block.h"
#include "graphics/resources.h"

void Blocks::Block::render(glm::vec3 const& pos, ResourceManager &resources) const {
	texture->enable(GL_TEXTURE0);
	shader->setUniformValue(3, 0);
	shader->setUniformValue(1, pos);
	model->render();
}
