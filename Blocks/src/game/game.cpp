#include "game/game.h"

#include <glm/glm.hpp>
#include <GL/glew.h>

#include <iostream>

Blocks::Game::Game(sf::Window const &window) : running(false), inputHandler(keys), camera(window.getSize()) {
}

void Blocks::Game::render(sf::Window &window) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	window.setMouseCursorVisible(!keys.mouse.right.isPressed());
	world.render(camera, resources);
	window.display();
}

void Blocks::Game::update(sf::Window &window) {
	world.update();

	float speed = 0.001f + 0.002f * keys.shift.isPressed();
	if (keys.up.isPressed()) camera.forward(speed);
	if (keys.down.isPressed()) camera.backward(speed);
	if (keys.right.isPressed()) camera.right(speed);
	if (keys.left.isPressed()) camera.left(speed);
	if (keys.space.isPressed()) camera.upward(speed);
	if (keys.ctrl.isPressed()) camera.downward(speed);

	if (keys.mouse.right.isPressed()) {
		sf::Vector2u s = window.getSize();
		sf::Vector2i p(s.x / 2, s.y / 2);

		float horizontalAngle = p.x - keys.mouse.pos.x;
		float verticalAngle = keys.mouse.pos.y - p.y;
		horizontalAngle *= 1.f / 400;
		verticalAngle *= 1.f / 400;
		camera.turn(horizontalAngle, verticalAngle);

		sf::Mouse::setPosition(p, window);
	}

}

bool Blocks::Game::isRunning() const {
	return running;
}

void Blocks::Game::start() {
	running = true;
}

void Blocks::Game::stop() {
	running = false;
}

void Blocks::Game::handleEvent(sf::Event const &event) {
	switch (event.type) {
	case sf::Event::Closed:
		stop();
		break;
	case sf::Event::Resized:
		camera.setScreenSize(event.size.width, event.size.height);
		glViewport(0, 0, event.size.width, event.size.height);
		break;
	default:
		inputHandler.handleEvent(event);
		break;
	}
}
