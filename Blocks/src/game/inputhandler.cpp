#include "game/inputhandler.h"

#include <algorithm>
#include <iostream>

Blocks::InputHandler::InputHandler(Keys &keys) : keys(&keys) {
	
	using sf::Keyboard;
	using sf::Mouse;

	keyBindings[Keyboard::W] = &keys.up;
	keyBindings[Keyboard::Up] = &keys.up;

	keyBindings[Keyboard::S] = &keys.down;
	keyBindings[Keyboard::Down] = &keys.down;

	keyBindings[Keyboard::A] = &keys.left;
	keyBindings[Keyboard::Left] = &keys.left;

	keyBindings[Keyboard::D] = &keys.right;
	keyBindings[Keyboard::Right] = &keys.right;

	keyBindings[Keyboard::Space] = &keys.space;

	keyBindings[Keyboard::LControl] = &keys.ctrl;
	keyBindings[Keyboard::RControl] = &keys.ctrl;

	keyBindings[Keyboard::LShift] = &keys.shift;
	keyBindings[Keyboard::RShift] = &keys.shift;

	mouseBindings[Mouse::Left] = &keys.mouse.left;

	mouseBindings[Mouse::Right] = &keys.mouse.right;
}

void Blocks::InputHandler::handleEvent(sf::Event const &event) {

	using sf::Event;

	switch (event.type) {
	case Event::KeyPressed:
		handleKeyPressedEvent(event);
		break;
	case Event::KeyReleased:
		handleKeyReleasedEvent(event);
		break;
	case Event::MouseMoved:
		handleMouseMovedEvent(event);
		break;
	case Event::MouseButtonPressed:
		handleMouseButtonPressedEvent(event);
		break;
	case Event::MouseButtonReleased:
		handleMouseButtonReleasedEvent(event);
		break;
	default:
		break;
	}
}

void Blocks::InputHandler::handleMouseMovedEvent(sf::Event const &event) {
	keys->mouse.pos.x = static_cast<float>(event.mouseMove.x);
	keys->mouse.pos.y = static_cast<float>(event.mouseMove.y);
}

void Blocks::InputHandler::handleMouseButtonPressedEvent(sf::Event const &event) {
	auto it = mouseBindings.find(event.mouseButton.button);
	if (it != mouseBindings.end()) {
		it->second->press();
	}
}

void Blocks::InputHandler::handleMouseButtonReleasedEvent(sf::Event const &event) {
	auto it = mouseBindings.find(event.mouseButton.button);
	if (it != mouseBindings.end()) {
		it->second->release();
	}
}

void Blocks::InputHandler::handleKeyPressedEvent(sf::Event const &event) {
	auto it = keyBindings.find(event.key.code);
	if (it != keyBindings.end()) {
		it->second->press();
	}
}

void Blocks::InputHandler::handleKeyReleasedEvent(sf::Event const &event) {
	auto it = keyBindings.find(event.key.code);
	if (it != keyBindings.end()) {
		it->second->release();
	}
}
