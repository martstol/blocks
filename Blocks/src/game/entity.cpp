#include "game/entity.h"
#include "game/world.h"

Blocks::Entity::Entity(glm::vec3 const &position, glm::vec2 const &direction) : position(position), direction(direction) {
	
}

void Blocks::Entity::update(World * world) {
	
}

void Blocks::Entity::render(glm::mat4 const &vp, ResourceManager &resources) {

}
