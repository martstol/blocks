#include "utils.h"

#include <lodepng.h>

#include <fstream>
#include <sstream>
#include <stdexcept>

std::string Blocks::readFile(std::string const& filename) {
	std::ifstream in(filename);
	if (in.fail()) {
		throw std::runtime_error("Could not open file: " + filename);
	}
	std::ostringstream ss;
	ss << in.rdbuf();
	return ss.str();
}

Blocks::Image Blocks::readImage(std::string const& filename) {
	Image img;
	lodepng::decode(img.pixels, img.width, img.height, filename, LCT_RGB);
	return img;
}
