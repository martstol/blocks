#include <iostream>
#include <stdexcept>

#include "game/game.h"
#include "graphics/glutils.h"
#include "graphics/framecounter.h"

int main(int argc, char* argv[]) {
	try {
		sf::Window window(sf::VideoMode(800, 600), "Blocks", sf::Style::Default);

		Blocks::initializeGlew();
		Blocks::setupGL();
		Blocks::printGLInfo();

		Blocks::Game game(window);
		game.start();

		Blocks::FrameCounter fpsCounter;
		while (game.isRunning()) {
			game.update(window);
			game.render(window);

			fpsCounter.update(window);

			sf::Event event;
			while (window.pollEvent(event)) {
				game.handleEvent(event);
			}
		}

		window.close();

	} catch (std::runtime_error &ex) {
		std::cout << ex.what() << std::endl;
		system("PAUSE");
	} catch (...) {
		std::cout << "An unexpected exception occured" << std::endl;
		system("PAUSE");
	}

	return 0;
}
